<?php

const MAX_SCORE = 21;
const CARDS_VALUES = [
    2 => 2,
    3 => 3,
    4 => 4,
    5 => 5,
    6 => 6,
    7 => 7,
    8 => 8,
    9 => 9,
    10 => 10,
    'J' => 11,
    'Q' => 11,
    'K' => 11,
    'A' => 1,
];

$cards = fillCards();

$stdin = fopen('php://stdin', 'r');

echo ">>> Player is picking cards.";
echo PHP_EOL;
echo ">>> Pick a card (yes/no)";

static $playerTotal = 0;
static  $AITotal = 0;

while (1) {
    $consoleText = trim(fgets($stdin));
    if('yes' === $consoleText){
        $playerSum = takeCard($cards);
        $playerTotal+= $playerSum;
        echo sprintf('You picked %d. Your score is %d.', $playerSum, $playerTotal);
        echo PHP_EOL;
        if(MAX_SCORE < $playerTotal){
            echo PHP_EOL;
            echo 'YOU LOST';
            echo PHP_EOL;
            echo 'Restart?';
            $playerTotal = 0;
            $AITotal = 0;
            $cards = fillCards();
            echo PHP_EOL;
            echo 'Type "yes to pick the card"';
            continue;
        }
    }

    $isAITakesCard = mt_rand(0, 1);
    if(0 === $isAITakesCard){
        echo ">>> AI is skipping.";
    }

    if(1 === $isAITakesCard){
        echo ">>> AI is picking cards.";
        echo PHP_EOL;
        $aiSum = takeCard($cards);
        $AITotal+=  $aiSum;
        echo sprintf('AI picked %d. His score is %d.', $aiSum, $AITotal);
        if(MAX_SCORE < $AITotal){
            echo PHP_EOL;
            echo 'AI LOST';
            echo PHP_EOL;
            $playerTotal = 0;
            $AITotal = 0;
            $cards = fillCards();
            echo PHP_EOL;
            echo 'Again? Type "yes to pick the card"';
            continue;
        }
    }

    echo PHP_EOL;
    echo ">>> Pick a card (yes/no)";
}

function takeCard(array &$cards){
    $lastKey =  $cards[count($cards) - 1];
    $score = CARDS_VALUES[$lastKey];
    unset($cards[$lastKey]);

    return $score;
}

function fillCards(){
    $cards = [];
    $totalCards = mt_rand(52, 416);
   // because task does not include how many cards of what type, we just take random amount of each card type
    for ($total =0; $total <= $totalCards; $total++) {
        $cards[] = array_rand(CARDS_VALUES);
    }
    return $cards;
}