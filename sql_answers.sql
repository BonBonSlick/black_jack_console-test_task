
-- Решение 1
SELECT t.id as id, t.amount as amount, t.created_at as created_at, w.owner_id as owner_id FROM transaction t INNER JOIN wallet w on t.wallet_id = w.id WHERE t.type = 'deposit' ORDER BY t.created_at ;

-- Решение 4
CREATE TABLE IF NOT EXISTS `owner` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `username` varchar(32) NOT NULL,
    `age` int DEFAULT NULL ,
    `created_at` datetime NOT NULL,
    `gender` varchar(1) DEFAULT 'M',
    `is_right_armed` BOOLEAN NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `id` (`id`),
    UNIQUE KEY `username` (`username`)
    ) ENGINE=InnoDB AUTO_INCREMENT=262354 DEFAULT CHARSET=latin1;
CREATE INDEX AGE_GENDER_SEARCH_INDEX ON `owner` (age,gender);
ALTER TABLE `owner` ADD  COLUMN owner_id int NOT NULL ;
ALTER TABLE `owner` ADD CONSTRAINT FK_13045942B FOREIGN KEY  (owner_id) REFERENCES wallet (id) ON DELETE  CASCADE ;

-- Решение 3
-- Ответ создан - Akina @Akina Сетевой и системный админ, SQL-программист.
SELECT usesr.id, SUM(CASE WHEN transaction.type IN ('deposit', 'adjustment_up',)
                          THEN amount
                          WHEN transaction.type IN ('adjustment_down', 'rollback')
                          THEN -amount
                          ELSE 0
                          END) balance
FROM user
JOIN wallet ON iser.id = wallet.user_id
JOIN transaction ON wallet.id = transaction.wallet_id
GROUP BY user.id